# Zadanie

Zadanie zrobione dla celów rekrutacyjnych.

## Wykonanie

### Założenia

Całe zadanie wrzuciełm do najnowszego Symfony (w tym momencie 5.2) i tam kontynuowałem pracę. Wykorzystałem biblioteki do walidacji schemy opis/json-schema oraz symfonowy serializer.

### Techniczne

DataProvider pobiera dane z pliku json oraz przypisaną mu schemę.
Wszystkie dane sa walidowane przez schemę.
ItemService korzystając z DataProvider zamienia dane na Entity.
Kontroler buduje BrandService za pomocą fabryki. Fabryka określa typ BrandService po rodzaju sortowania.

## Instalacja

composer install
