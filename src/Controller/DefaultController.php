<?php

namespace App\Controller;

use App\Service\BrandServiceFactory;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    public function index(BrandServiceFactory $brandServiceFactory): JsonResponse
    {
        try {
            $brandService = $brandServiceFactory->getBrandService(BrandServiceFactory::SORT_BY_NAME);
            $items = $brandService->getItemsForCollection('winter');
        } catch (\Exception $ex) {
            return new JsonResponse($this->getErrorResponse($ex->getMessage()));
        }
        return new JsonResponse($this->getSuccessResponse($items));
    }

    private function getSuccessResponse(array $items): array
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($items, 'json');
        return ['items' => json_decode($jsonContent, true)];
    }

    private function getErrorResponse(string $message)
    {
        return ['error' => $message];
    }
}