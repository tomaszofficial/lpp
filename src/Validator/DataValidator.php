<?php

namespace App\Validator;

use App\Exception\ServiceException;
use Opis\JsonSchema\{
    Validator, ValidationResult, ValidationError, Schema
};

class DataValidator implements ValidatorInterface
{
    private $jsonSchema;
    private $jsonData;

    public function __construct(string $jsonSchema, string $jsonData)
    {
        $this->jsonSchema = $jsonSchema;
        $this->jsonData = $jsonData;
    }

    public function validate(): bool
    {
        $data = \json_decode($this->jsonData);
        $schema = Schema::fromJsonString($this->jsonSchema);

        $validator = new Validator();
        /** @var ValidationResult $result */
        $result = $validator->schemaValidation($data, $schema);

        if ($result->isValid()) {
            return true;
        } else {
            /** @var ValidationError $error */
            $error = $result->getFirstError();
            $message = 'Data is invalid. Error: ' . $error->keyword() . \json_encode($error->keywordArgs());
            throw new ServiceException($message);
        }
    }
}