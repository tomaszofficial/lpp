<?php

namespace App\DataProvider;

class DataProvider
{
    private const PATH_TO_DATA_FILE = __DIR__ . '/../../data/1315475.json';
    private const PATH_TO_SCHEMA_FILE = __DIR__ . '/../../schema/1315475.json';

    public function getJsonData(): string
    {
        return file_get_contents(self::PATH_TO_DATA_FILE);
    }

    public function getJsonSchema(): string
    {
        return file_get_contents(self::PATH_TO_SCHEMA_FILE);
    }
}