<?php

namespace App\Service;

use App\Entity\Brand;

class UnorderedBrandService extends AbstractBrandService
{
    public function getItemsForCollection(string $collectionName): array
    {
        $items = [];
        $brands = $this->getBrandsForCollection($collectionName);
        foreach ($brands as $brand) {
            $items += $brand->getItems();
        }
        return $items;
    }
}
