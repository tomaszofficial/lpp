<?php

namespace App\Service;

use App\Entity\Item;

class PriceOrderedBrandService extends AbstractBrandService
{
    public function getItemsForCollection(string $collectionName): array
    {
        $items = [];
        $brands = $this->getBrandsForCollection($collectionName);
        foreach ($brands as $brand) {
            $items += $brand->getItems();
        }
        usort($items, function (Item $a, Item $b) {
            return strcmp(current($b->getPrices())->getPriceInEuro(), current($a->getPrices())->getPriceInEuro());
        });
        return $items;
    }
}