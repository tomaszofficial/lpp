<?php

namespace App\Service;

interface BrandServiceFactoryInterface
{
    public function getBrandService(string $sortType): BrandServiceInterface;
}