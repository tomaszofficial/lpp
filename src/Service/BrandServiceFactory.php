<?php

namespace App\Service;

class BrandServiceFactory implements BrandServiceFactoryInterface
{
    public const SORT_BY_NAME = 'name';
    public const SORT_BY_PRICE = 'price';

    private $itemService;

    public function __construct(ItemServiceInterface $itemService)
    {
        $this->itemService = $itemService;
    }

    public function getBrandService(?string $sortType): BrandServiceInterface
    {
        switch ($sortType) {
            case 'name':
                return new ItemNameOrderedBrandService($this->itemService);
            case 'price':
                return new PriceOrderedBrandService($this->itemService);
            default:
                return new UnorderedBrandService($this->itemService);
        }
    }
}