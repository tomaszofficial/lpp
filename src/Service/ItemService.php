<?php

namespace App\Service;

use App\DataProvider\DataProvider;
use App\Entity\Brand;
use App\Validator\DataValidator;

class ItemService implements ItemServiceInterface
{
    private $dataProvider;

    public function __construct(DataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    public function getResultForCollectionId(int $collectionId): array
    {
        $brandsEntities = [];
        $data = $this->dataProvider->getJsonData();
        $schema = $this->dataProvider->getJsonSchema();
        $dataValidator = new DataValidator($schema, $data);
        if ($dataValidator->validate()) {
            $arrayData = \json_decode($data, true);
            $wantedCollection = $arrayData['id'] === $collectionId ? $arrayData['brands'] : [];
            foreach ($wantedCollection as $brandId => $brand) {
                $brandsEntities[$brandId] = Brand::create($brand);
            }
        }
        return $brandsEntities;
    }
}