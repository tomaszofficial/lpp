<?php

namespace App\Service;

use App\Entity\Item;

class ItemNameOrderedBrandService extends AbstractBrandService
{
    public function getItemsForCollection(string $collectionName): array
    {
        $items = [];
        $brands = $this->getBrandsForCollection($collectionName);
        foreach ($brands as $brand) {
            $items += $brand->getItems();
        }
        usort($items, function (Item $a, Item $b) {
            return strcmp($a->getName(), $b->getName());
        });
        return $items;
    }
}