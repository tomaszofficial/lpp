<?php

namespace App\Entity;

use App\Traits\CreateItem;

/**
 * Represents a single item from a search result.
 * 
 */
class Item
{
    use CreateItem;

    /**
     * Name of the item
     *
     * @var string
     */
    private $name;

    /**
     * Url of the item's page
     * 
     * @var string
     */
    private $url;

    /**
     * Unsorted list of prices received from the 
     * actual search query.
     * 
     * @var Price[]
     */
    private $prices;

    public function __construct() {
        $this->prices = [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return Price[]
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * @param Price[] $prices
     */
    public function setPrices(array $prices): self
    {
        $this->prices = $prices;
        return $this;
    }
}
