<?php

namespace App\Entity;

use App\Traits\CreateBrand;

/**
 * Represents a single brand in the result.
 *
 */
class Brand
{
    use CreateBrand;

    /**
     * Name of the brand
     *
     * @var string
     */
    private $brand;

    /**
     * Brand's description
     * 
     * @var string
     */
    private $description;

    /**
     * Unsorted list of items with their corresponding prices.
     * 
     * @var Item[]
     */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): self
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }
}
