<?php

namespace App\Traits;

use App\Entity\Price;

trait CreatePrice
{
    public static function create(array $price): Price {
        $description = $price['description'];
        $priceInEuro = $price['priceInEuro'];
        $arrivalDate = new \DateTime($price['arrival']);
        $dueDate = new \DateTime($price['due']);

        return (new Price())
            ->setDescription($description)
            ->setPriceInEuro($priceInEuro)
            ->setArrivalDate($arrivalDate)
            ->setDueDate($dueDate);
    }
}