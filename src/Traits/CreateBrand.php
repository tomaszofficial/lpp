<?php

namespace App\Traits;

use App\Entity\Brand;
use App\Entity\Item;

trait CreateBrand
{
    public static function create(array $brand) : Brand {
        $brandProperty = $brand['name'];
        $description = $brand['description'];
        $items = array_map(function (array $item) {
            return Item::create($item);
        }, $brand['items']);

        return (new Brand())
            ->setBrand($brandProperty)
            ->setDescription($description)
            ->setItems($items);
    }
}