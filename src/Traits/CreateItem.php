<?php

namespace App\Traits;

use App\Entity\Item;
use App\Entity\Price;

trait CreateItem
{
    public static function create(array $items): Item {
        $name = $items['name'];
        $url = $items['url'];
        $prices = array_map(function (array $price) {
            return Price::create($price);
        }, $items['prices']);

        return (new Item())
            ->setName($name)
            ->setUrl($url)
            ->setPrices($prices);
    }
}